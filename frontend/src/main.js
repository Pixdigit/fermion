import Vue from "vue";
import App from "./App.vue";

Vue.config.productionTip = false;
Vue.config.devtools = true;

import * as Wails from "@wailsapp/runtime";
import "./assets/sass/main.sass";
import i18n from "./i18n";
import AllMdIcon from "vue-ionicons/dist/ionicons-md.js";

Vue.use(AllMdIcon);

//Custom event bubbling
Vue.use(Vue => {
	Vue.prototype.$bubble = function $bubble(eventName, ...args) {
		let component = this;
		do {
			component.$emit(eventName, ...args);
			component = component.$parent;
		} while (component);
	};
});
Wails.Init(() => {
	const vm = new Vue({
		i18n,
		render: h => h(App)
	}).$mount("#app");
});
console.log(Wails);
