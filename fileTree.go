package main

import (
	"fmt"
	"io/ioutil"
	"os"
	"path/filepath"
	"sort"
	"strings"
)

type fileInfo struct {
	AbsPath  string
	Name     string
	IsDir    bool
	Children []string
}

func OpenFile(path string) (string, error) {
	bytes, err := ioutil.ReadFile(path);	if err != nil {return "", fmt.Errorf("Error opening file: %w", err)}
	return string(bytes), nil
}

func LookupFilesystem(filePath string) (fileInfo, error) {
	path, err := filepath.Abs(filePath);	if err != nil {return fileInfo{}, fmt.Errorf("Cannot determine absolute path: %w", err)}
	file, err := os.Open(path);	if err != nil {return fileInfo{}, fmt.Errorf("Error opening Path %v: %w", path, err)}
	defer file.Close()
	stat, err := file.Stat()

	children := []string{}
	if stat.IsDir() {
		childrenInfo, err := file.Readdir(0);	if err != nil {return fileInfo{}, fmt.Errorf("Error getting children of directory: %w", err)}
		sort.Slice(childrenInfo, func(i int, j int) bool {
			var isLess bool
			if childrenInfo[i].IsDir() != childrenInfo[j].IsDir() {
				isLess = childrenInfo[i].IsDir()
			} else {
				//i less than j lexicographically
				isLess = strings.Compare(strings.ToLower(childrenInfo[i].Name()), strings.ToLower(childrenInfo[j].Name())) == -1
			}
			return isLess
		})

		for _, child := range childrenInfo {
			fullPath, err := filepath.Abs(filepath.Join(path, child.Name()));	if err != nil {return fileInfo{}, fmt.Errorf("Could not determing child's full path: %w", err)}
			children = append(children, fullPath)
		}
	}

	tree := fileInfo{
		path,
		stat.Name(),
		stat.IsDir(),
		children,
	}
	return tree, nil
}
