# Fermion

### What is Fermion?
Fermion is a new code editor based on a Javascript/Vue frontend with a Go backend. The name is derived from Atom as Fermion is intended to be a simpler, more broken down version of Atom (that is also not owned by a company). Why this tech-stack? Because I like it. Besides it is very OS and platform agnostic. While I agree that javascript is comparatively resource hungry, I value the ease of use from Javascript and Vue over e.g. GTK or QT. Besides as little computation is done in the frontend it should also be able to run on less powerful machines.

### Getting started
This Project uses the fantastic [Wails](https://wails.app). Basically think electron but with a Go backend. See the [Wails installation instructions](https://wails.app/gettingstarted/) on how to install. This should also get Node and Go for you. Make sure Wails is in your $PATH variable so you can use the wails CLI.

When you want to develop for the frontend you can just run `wails serve` in the project directory. This should start a local server with the backend running. To start the frontend run `npm run serve` in the `frontend` directory which you should then be able to reach on [localhost:8080](http://localhost:8080). Changes to the frontend should be automatically build and the frontend should reload. Sometimes, however, you need to refresh the tab for things to work. Also, changes to the backend are not build automatically, so you need to stop wails and run `wails serve` again.

If you plan to do more work on the backend I made a small shell script `debug.sh`. On other platforms just run `wails build -d` which will produce a binary you can then use to test. When building you also don't need to build the frontend separately as it is bundled by wails into the binary.

### Building Fermion
Building Fermion should be simple. Just [install Wails](https://wails.app/gettingstarted/) and run `wails build`. Make sure you have wails in your $PATH.
