package main

import (
	"fmt"
	"io/ioutil"
	"path/filepath"

	"github.com/leaanthony/mewn"
	"github.com/wailsapp/wails"
)

type Runtime struct {
	runtime *wails.Runtime
}

func Log(data ...interface{})  {
	fmt.Println("Log: \"" + fmt.Sprint(data) + "\"")
}

func (r *Runtime) WailsInit(runtime *wails.Runtime) error {
	runtime.Events.On("test", Log)
	r.runtime = runtime
	return nil
}

func (r *Runtime) WailsShutdown() {
}

func (r *Runtime) Quit() {
	fmt.Println("Goodbye!")
	r.runtime.Window.Close()
}

func SaveFile(path, content string) error {
	absPath, err := filepath.Abs(path);	if err != nil {return fmt.Errorf("Can not write file: %w", err)}
	err = ioutil.WriteFile(absPath, []byte(content), 0774);	if err != nil {return fmt.Errorf("Can not write file: %w", err)}
	return nil
}

func main() {
	js := mewn.String("./frontend/dist/app.js")
	css := mewn.String("./frontend/dist/app.css")

	app := wails.CreateApp(&wails.AppConfig{
		Width:  1024,
		Height: 768,
		Title:  "Fermion",
		JS:     js,
		CSS:    css,
	})
	app.Bind(&Runtime{})
	app.Bind(LookupFilesystem)
	app.Bind(OpenFile)
	app.Bind(SaveFile)
	app.Run()
}
