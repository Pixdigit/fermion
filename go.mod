module fermion

require (
	github.com/Masterminds/semver v1.5.0 // indirect
	github.com/dchest/htmlmin v1.0.0 // indirect
	github.com/gorilla/websocket v1.4.1 // indirect
	github.com/leaanthony/mewn v0.10.7
	github.com/mattn/go-colorable v0.1.4 // indirect
	github.com/mattn/go-isatty v0.0.11 // indirect
	github.com/sirupsen/logrus v1.4.2 // indirect
	github.com/wailsapp/wails v1.0.1
	golang.org/x/net v0.0.0-20191209160850-c0dbc17a3553 // indirect
	golang.org/x/sys v0.0.0-20191210023423-ac6580df4449 // indirect
	golang.org/x/text v0.3.2 // indirect
	gopkg.in/yaml.v3 v3.0.0-20191120175047-4206685974f2 // indirect
)

go 1.13
